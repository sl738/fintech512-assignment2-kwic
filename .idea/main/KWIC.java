import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class KWIC {
    public List<String> words_ignore;
    public List<String> words_title;
    public ArrayList<ArrayList<String>> words;

    KWIC(String input) {
        this.words_ignore = new ArrayList<String>();
        this.words_title = new ArrayList<String>();
        this.words = new ArrayList<List<String>>();
        String[] separate = input.split("\n");
        //create commands for deleting, extracting words from inputs//
        //also create a string list to separate the inputs//

        for (String input_ignore : separate) {
            this.words_ignore.add(input_ignore);
        }

        for (String input_titles : separate) {
            if (input_titles != "") {
                this.words_title.add(input_titles);
            }
        }
    }

    public String get_ignore() {
        String list_ignore = "";
        for (String i : words_ignore) {
            list_ignore = list_ignore + i + "\n";
        }
        return list_ignore;
    }

    public String get_title() {
        String list_title = "";
        for (String i : words_title) {
            list_title = list_title + i + "\n";
        }
        return list_title;
    }

    public static ArrayList<String> getKeyword(ArrayList<String> KWIC) {
        ArrayList<String> keyword = new ArrayList<>();
        ArrayList<String> list_ignore = get_ignore(KWIC);
        ArrayList<String> list_title = get_title(KWIC);
        for (int i = 0; i < list.title.size(); i++) {
            String end_title = list_title.get(i);

            keyword.addAll(Arrays.asList(end_title.split(" ")));

            for (int j = 0; j < list_ignore.size(); j++) {
                for (int x = 0; x < keyword.size(); x++) {
                    if (keyword.get(x).equalsIgnoreCase(list_ignore.get(j))) {
                        keyword.remove(x);
                    }
                }
            }
        }
        HashSet<String> set = new HashSet<>(keyword);
        keyword.clear();
        keyword.addAll(set);
        keyword.sort(String::compareToIgnoreCase);

        return keyword;
    }

    public static ArrayList<String> Output(ArrayList<String> input) {
        ArrayList<String> output = new ArrayList<>();
        ArrayList<String> titles = get_title(input);
        ArrayList<String> keywords = getKeyword(input);

        for (int i = 0; i < keywords.size(); i++) {
            String keyword = keywords.get(i);
            for (int j = 0; j < titles.size(); j++) {
                String title = titles.get(j);
                if (title.contains(keyword)) {
                    int count = 0;
                    Pattern p = Pattern.compile(keyword);
                    Matcher m = p.matcher(title);
                    while (m.find()) {
                        count++;
                    }

                    int indx = title.indexOf(keyword);
                    ArrayList<Integer> indxs = new ArrayList<>();
                    indxs.add(indx);
                    while (indx >= 0) {
                        indx = title.indexOf(keyword, indx + 1);
                        indxs.add(indx);
                    }

                    for (int c = 0; c < indxs.size() - 1; c++) {

                        if (indxs.get(c) != 0) {
                            title = title.toLowerCase();
                            title = title.replace(keyword.toUpperCase(), keyword.toLowerCase());
                            title = title.substring(0, indxs.get(c)) + keyword.toUpperCase() + title.substring(indxs.get(c) + keyword.length(), title.length());
                        } else {
                            title = title.toLowerCase();
                            title = title.replaceFirst(keyword.toLowerCase(), keyword.toUpperCase());

                        }
                        output.add(title);

                    }

                }

            }

        }
        return output;
    }

    public static void main(String[] args) {

        ArrayList<String> input = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        String s = "";

        while (sc.hasNextLine()) {
            s = sc.nextLine();
            if (s.equals("exit")) {
                break;
            }
            input.add(s);
        }
        sc.close();
        ArrayList<String> output = Output(input);
        for (int i = 0; i < input.size(); i++) {
            System.out.println(output.get(i));
        }

    }

}
